#Ho Ten: Lương Thiện Hưng

#MSSV: 2180607587

#Lớp: 21DTHD4
#Khoa: Công Nghệ Thông Tin
| Title              | Rank students based on average score	                   |
|:-------------------:|:-----------------------------------------------|
| Value Statement     |   Rank students based on average score	|
| Acceptence Criter   | Acceptence Criterion 1: Displays the average score of each student
|                     |  Acceptence Criterion 2: Displays the rank and title of each student
| Definition Of Done  | Know the percentage of rankings|
| Owner               | Thien Hung                       | 

